#include "ar1/execution.hpp"
#include "ar1/task.hpp"
#include "ar1/qt/context.hpp"
#include "ar1/qt/signal_awaiter.hpp"
#include <QApplication>
#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <iostream>





class main_window : public QWidget
{
    Q_OBJECT
public:
    main_window(QWidget* parent = nullptr)
        : QWidget(parent)
    {
        m_label = new QLabel{"Label"};
        m_button = new QPushButton{"Touch me"};

        QVBoxLayout* main_layout = new QVBoxLayout{};
        main_layout->addWidget(m_label);
        main_layout->addWidget(m_button);
        setLayout(main_layout);
    }

    void closeEvent([[maybe_unused]] QCloseEvent* event)
    {
        emit closed();
    }

signals:
    void closed();

private:
    QLabel* m_label;
    QPushButton* m_button;
};


ar1::task<void> main_task()
{
    main_window w;
    w.show();

    co_await ar1::qt::signal_awaiter{&w, &main_window::closed};

    co_return;
}


int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    QApplication application(argc, argv);
    application.setQuitOnLastWindowClosed(false);
    ar1::qt::context c;
    ar1::default_executor_set(c);

    auto test = main_task();
    ar1::await(test, ar1::functor_with_executor{[&](){ application.exit(); }, c});

    application.exec();

    return 0;
}


#include "pg_qt.moc"
