#include "ar1/execution.hpp"
#include "ar1/task.hpp"
#include "ar1/boost/thread.hpp"
#include "../tests/lib/atest.hpp"
#include "ar1/boost/awaitable_timer.hpp"
#include "ar1/any_awaitable.hpp"
#include <iostream>
#include <optional>


using namespace ar1;






ar1::task<void> main_task()
{
    //basio::awaitable_timer tmr(100);

    std::cout << "1" << std::endl;

    any_awaitable<void> aw = make_any_awaitable<basio::awaitable_timer>(500);
    co_await aw;

    std::cout << "2" << std::endl;


    co_return;
}



int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    basio::thread loop;
    loop.set_name("main");
    ar1::default_executor_set(loop);

    auto all = main_task();
    ar1::await(all, ar1::functor_with_executor{[&](){ loop.stop(); }, loop});

    loop.run_for(std::chrono::seconds(5));
    AR1_ASSERT(all.finished());

    return 0;
}
