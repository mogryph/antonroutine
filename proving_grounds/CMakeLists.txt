cmake_minimum_required(VERSION 3.5)

project(antonroutine-proving-grounds LANGUAGES CXX)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Widgets)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Widgets)


function(make_pg name file)
    add_executable(${name} ${file})
    target_compile_options(${name} PRIVATE -Wall -Wextra -Wpedantic)
    target_include_directories(${name} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/lib)
    target_link_libraries(${name} antonroutine)
endfunction()


if (ANTONROUTINE_PROVING_GROUNDS)
    make_pg(pg_polygon pg_polygon.cpp)
    target_link_libraries(pg_polygon antonroutine_integration_boost)
    make_pg(pg_qt pg_qt.cpp)
    target_link_libraries(pg_qt antonroutine_integration_qt Qt${QT_VERSION_MAJOR}::Widgets)
endif ()
