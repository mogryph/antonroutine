#pragma once


namespace boost::asio
{
} // namespace boost::asio

namespace boost::asio::experimental
{
} // namespace boost::asio::experimental

namespace basio
{
using namespace boost::asio;
using namespace boost::asio::experimental;
} // namespace basio
