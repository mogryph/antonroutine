#pragma once

#include "thread.hpp"
#include "ar1/awaitable.hpp"
#include <boost/asio/steady_timer.hpp>
#include <coroutine>


namespace basio
{

class awaitable_timer
{
public:
    ~awaitable_timer()
    {
        m_timer.cancel();
    }

    explicit awaitable_timer(int msecs)
        : m_timer(thread::current_thread())
        , m_msecs(msecs)
    {
    }

    awaitable_timer(const awaitable_timer& other) = delete;
    awaitable_timer(awaitable_timer&& other) noexcept = delete;
    awaitable_timer& operator=(const awaitable_timer& other) = delete;
    awaitable_timer& operator=(awaitable_timer&& other) noexcept = delete;

    [[nodiscard]] bool await_ready() const
    {
        return false;
    }

    template <typename Continuation>
    [[nodiscard]] bool await_suspend(Continuation&& continuation_)
    {
        m_continuation = std::forward<Continuation>(continuation_);

        m_timer.expires_from_now(std::chrono::milliseconds(m_msecs));
        m_timer.async_wait([this](const boost::system::error_code& e)
        {
            if (!e)
            {
                resume();
            }
        });

        return true;
    }

    void await_resume()
    {
    }

    [[nodiscard]] bool finished() const
    {
        return m_c;
    }

    void cancel()
    {
        m_timer.cancel();
        m_c = true;
        resume();
    }

private:
    void resume()
    {
        m_continuation();
    }

    ar1::continuation m_continuation;
    basio::steady_timer m_timer;
    int m_msecs;
    bool m_c = false; // TODO REM
};

static_assert(ar1::UnifiedAwaitable<awaitable_timer>);


} // namespace basio
