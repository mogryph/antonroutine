#pragma once

#include "basio.hpp"
#include "ar1/execution.hpp"
#include <boost/asio/io_context.hpp>
#include <boost/asio/steady_timer.hpp>
#include <functional>


namespace basio
{

class thread : public io_context, public ar1::execution_context
{
public:

    ~thread()
    {
        m_current_thread = nullptr;
    }

    thread()
        : m_exit_timer(*this)
    {
        m_exit_timer.expires_from_now(std::chrono::hours(1));
        m_exit_timer.async_wait([](boost::system::error_code){});

        m_current_thread = this;
    }

    static thread& current_thread()
    {
        return *m_current_thread;
    }

    thread(const thread& other) = delete;
    thread& operator=(const thread& other) = delete;

    void execute(std::function<void ()> f) override
    {
        post(std::move(f));
    }

    void set_name(std::string name)
    {
        m_name = std::move(name);
    }

private:
    thread_local static thread* m_current_thread;
    basio::steady_timer m_exit_timer;
    std::string m_name;
};

}
