#ifndef AR1_QT_SIGNAL_AWAITER_HPP
#define AR1_QT_SIGNAL_AWAITER_HPP

#include "ar1/awaitable.hpp"
#include <QApplication>
#include <QObject>


namespace ar1::qt
{

template <typename OType, typename MType>
class signal_awaiter
{
public:
    ~signal_awaiter()
    {
        QObject::disconnect(m_connection);
    }

    signal_awaiter(OType* o, MType m)
    {
        m_connection = QObject::connect(o, m, [this](){ resume(); });
    }

    [[nodiscard]] bool await_ready() const
    {
        return false;
    }

    [[nodiscard]] bool await_suspend(std::coroutine_handle<> handle)
    {
        AR1_ASSERT(!m_handle);
        m_handle = std::move(handle);

        return true;
    }

    void await_resume()
    {

    }

private:
    void resume()
    {
        std::coroutine_handle<> handle;
        std::swap(m_handle, handle);
        if (handle)
        {
            handle();
        }
    }

    QMetaObject::Connection m_connection;
    std::coroutine_handle<> m_handle;
};


} // namespace ar1::qt


#endif // AR1_QT_SIGNAL_AWAITER_HPP
