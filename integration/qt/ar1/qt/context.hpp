#ifndef AR1_QT_CONTEXT_HPP
#define AR1_QT_CONTEXT_HPP

#include "ar1/execution.hpp"
#include <QApplication>


namespace ar1::qt
{


class context : public ar1::execution_context
{
public:
    virtual void execute(std::function<void ()> f)
    {
        QMetaObject::invokeMethod(qApp, std::move(f));
    }
};


} // namespace ar1::qt


#endif // AR1_QT_CONTEXT_HPP
