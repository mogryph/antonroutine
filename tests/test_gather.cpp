#include "ar1/boost/thread.hpp"
#include "ar1/assert.hpp"
#include "ar1/task.hpp"
#include "elapsed_timer.hpp"
#include "sequence_generator.hpp"
#include "ar1/gather.hpp"
#include "atest.hpp"
#include <iostream>

using namespace ar1;




static task<void> test_task1()
{
    auto gen1 = sequence_generator_nt(3);
    auto gen2 = sequence_generator_nt(3);

    gather g(gen1, gen2);

    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 0 && std::get<0>(*r) == 0);
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 0 && std::get<0>(*r) == 1);
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 0 && std::get<0>(*r) == 2);
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 0 && !std::get<0>(*r));
    }

    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 1 && std::get<1>(*r) == 0);
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 1 && std::get<1>(*r) == 1);
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 1 && std::get<1>(*r) == 2);
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 1 && !std::get<1>(*r));
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(!r);
    }

    co_return;
}

static task<void> test_task2()
{
    auto gen1 = sequence_generator_d(3);
    auto gen2 = sequence_generator_d(3);

    gather g(gen1, gen2);

#if 0
    while (!g.finished())
    {
        auto r = co_await g;
        if (r)
        {
            std::cout << (*r).index() << std::endl;
        }
        else
        {
            std::cout << "FIN" << std::endl;
        }
    }
#else
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 0 && std::get<0>(*r) == 0);
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 1 && std::get<1>(*r) == 0);
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 0 && std::get<0>(*r) == 1);
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 1 && std::get<1>(*r) == 1);
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 0 && std::get<0>(*r) == 2);
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 1 && std::get<1>(*r) == 2);
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 0 && !std::get<0>(*r));
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(r && (*r).index() == 1 && !std::get<1>(*r));
    }
    {
        auto r = co_await g;
        ATEST_ASSERT(!r);
    }
#endif

    co_return;
}


static task<void> all_tests()
{
    co_await test_task1();
    co_await test_task2();
}


int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    basio::thread loop;
    loop.set_name("main");
    ar1::default_executor_set(loop);

    auto all = all_tests();
    ar1::await(all, ar1::functor_with_executor{[&](){ loop.stop(); }, loop});

    loop.run_for(std::chrono::seconds(10));
    AR1_ASSERT(all.finished());;

    return 0;
}
