#include "ar1/boost/thread.hpp"
#include "ar1/task.hpp"
#include "elapsed_timer.hpp"
#include "atest.hpp"
#include <iostream>

using namespace ar1;


using uptr = std::unique_ptr<int>;

uptr NONCONST_UPTR(new int(1111)); // NOLINT
const uptr CONST_UPTR(new int(2222));


task<uptr> test_yield_rval()
{
    co_yield uptr(new int(3333));
}

task<uptr&> test_yield_ref()
{
    co_yield NONCONST_UPTR;
}

task<const uptr&> test_yield_cref()
{
    co_yield CONST_UPTR;
}

task<uptr> test_return_rval()
{
    co_return uptr(new int(4444));
}

task<uptr&> test_return_ref()
{
    co_return NONCONST_UPTR;
}

task<const uptr&> test_return_cref()
{
    co_return CONST_UPTR;
}


static task<void> all_tests()
{
    {
        auto t = test_yield_rval();
        auto& r = co_await t;
        ATEST_ASSERT(*r == 3333);
        ar1_finish(t);
    }
    {
        auto t = test_yield_ref();
        auto& r = co_await t;
        ATEST_ASSERT(*r == 1111);
        *NONCONST_UPTR = 2222;
        ATEST_ASSERT(*r == 2222);
        *r = 3333;
        ATEST_ASSERT(*NONCONST_UPTR == 3333);
        ar1_finish(t);
    }
    {
        auto t = test_yield_cref();
        auto& r = co_await t;
        ATEST_ASSERT(*r == 2222);
        ar1_finish(t);
    }
    {
        auto t = test_return_rval();
        auto& r = co_await t;
        ATEST_ASSERT(*r == 4444);
        //co_await finish(t);
    }
    {
        auto t = test_return_ref();
        auto& r = co_await t;
        ATEST_ASSERT(*r == 3333);
        //co_await finish(t);
    }
    {
        auto t = test_return_cref();
        auto& r = co_await t;
        ATEST_ASSERT(*r == 2222);
        //co_await finish(t);
    }
}


int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    basio::thread loop;
    loop.set_name("main");
    ar1::default_executor_set(loop);

    auto all = all_tests();
    ar1::await(all, ar1::functor_with_executor{[&](){ loop.stop(); }, loop});

    loop.run_for(std::chrono::seconds(10));
    AR1_ASSERT(all.finished());;

    return 0;
}
