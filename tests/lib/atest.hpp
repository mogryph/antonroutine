#ifndef ATEST_HPP
#define ATEST_HPP

#include "elapsed_timer.hpp"
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <iomanip>


#define ATEST_ASSERT(cond)                                          \
    do                                                              \
    {                                                               \
        if (!(cond)) [[unlikely]]                                   \
        {                                                           \
            std::cerr << "ASSERTION FAILED:" << std::endl;          \
            std::cerr << __FILE__ << ":" << __LINE__ << std::endl;  \
            std::cerr << #cond << std::endl;                        \
            std::terminate();                                       \
        }                                                           \
    } while(false);                                                 \


#define ATEST_CHECK(v, e)                                   \
do                                                          \
    {                                                       \
            if ((v) != (e)) [[unlikely]]                       \
        {                                                   \
                std::stringstream ss;                       \
                ss << "ASSERTION FAILED: ";                 \
                ss << v << " != " << e << " ";   \
                throw std::runtime_error(ss.str());         \
        }                                                   \
} while(false);


#define ATEST_MUST_THROW(atest_e)       \
{                                       \
    bool atest_throw = false;           \
    try                                 \
    {                                   \
        {atest_e}                       \
    }                                   \
    catch (const std::exception& e)     \
    {                                   \
        atest_throw = true;             \
    }                                   \
    ATEST_ASSERT(atest_throw);          \
}


#define ATEST_TIME(atest_name, atest_e)                                                     \
{                                                                                           \
    bra::elapsed_timer atest_timer;                                                         \
    atest_timer.start();                                                                    \
    {atest_e}                                                                               \
    auto atest_elapsed = atest_timer.elapsed<std::chrono::milliseconds>();                  \
    std::cout << std::setw(40) << atest_name << ": " << atest_elapsed << std::endl;         \
}


#endif // ATEST_HPP
