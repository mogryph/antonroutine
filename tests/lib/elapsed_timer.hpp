#ifndef BRA_ELAPSED_TIMER_HPP
#define BRA_ELAPSED_TIMER_HPP

#include <chrono>


namespace bra
{


class elapsed_timer
{
public:

    elapsed_timer() = default;

    void start()
    {
        m_start = std::chrono::steady_clock::now();
    }

    template <typename T>
    T elapsed()
    {
        return std::chrono::duration_cast<T>(std::chrono::steady_clock::now() - m_start);
    }

private:
    std::chrono::time_point<std::chrono::steady_clock> m_start;
};


} // namespace bra


#endif // BRA_ELAPSED_TIMER_HPP
