#ifndef DESTROY_CALL_HPP
#define DESTROY_CALL_HPP

#include <iostream>


template <typename Functor>
class destroy_call
{
public:

    ~destroy_call()
    {
        m_f();
    }

    destroy_call(Functor f)
        : m_f(std::move(f))
    {

    }

private:
    Functor m_f;
};


#endif // DESTROY_CALL_HPP
