#pragma once

#include "ar1/awaitable.hpp"
#include "ar1/boost/awaitable_timer.hpp"
#include <optional>


namespace ar1
{

inline task<std::uint64_t> sequence_generator(std::size_t n)
{
    for (std::uint64_t i = 1; i < n; ++i)
    {
        co_yield i;
    }

    co_return 0;
}

inline task<std::optional<std::uint64_t>> sequence_generator_d(std::size_t n)
{
    basio::awaitable_timer tmr(50);

    for (std::uint64_t i = 0; i < n; ++i)
    {
        co_await tmr;
        co_yield i;
    }

    co_return std::nullopt;
}

class sequence_generator_nt
{
public:

    sequence_generator_nt(std::size_t n)
        : m_n(n)
        , m_c(0)
    {
    }

    sequence_generator_nt(const sequence_generator_nt& other) = delete;
    sequence_generator_nt(sequence_generator_nt&& other) noexcept = delete;
    sequence_generator_nt& operator=(const sequence_generator_nt& other) = delete;
    sequence_generator_nt& operator=(sequence_generator_nt&& other) noexcept = delete;

    bool await_ready() const
    {
        return true;
    }

    template <typename Continuation>
    bool await_suspend([[maybe_unused]] Continuation continuation_)
    {
        std::terminate();
    }

    std::optional<std::uint64_t> await_resume()
    {
        if (m_c >= m_n)
        {
            m_c++;
            return std::nullopt;
        }
        else
            return m_c++;
    }

    bool finished() const
    {
        return m_c >= m_n + 1;
    }

    void cancel()
    {

    }

private:
    std::size_t m_n;
    std::uint64_t m_c;
};

static_assert(UnifiedAwaitable<sequence_generator_nt>);

}
