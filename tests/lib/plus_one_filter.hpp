#pragma once

#include "ar1/task.hpp"
#include <optional>


namespace ar1
{

template <typename Source>
inline task<std::uint64_t> plus_one_filter(Source& source)
{
    while (auto v = co_await source)
    {
        co_yield v + 1;
    }

    co_return 0;
}

template <typename Source>
class plus_one_filter_nt
{
public:
    plus_one_filter_nt(Source& source)
        : m_source(source)
    {
    }

    bool await_ready()
    {
        return m_source.await_ready();
    }

    template <typename Continuation>
    bool await_suspend(Continuation&& continuation_)
    {
        return m_source.await_suspend(std::forward<Continuation>(continuation_));
    }

    std::optional<std::uint64_t> await_resume()
    {
        auto v = m_source.await_resume();
        if (v)
        {
            return *v + 1;
        }
        else
        {
            m_finished = true;
            return std::nullopt;
        }
    }

    bool finished() const
    {
        return m_finished;
    }

private:
    Source& m_source;
    bool m_finished = false;
};

}
