#include "ar1/boost/thread.hpp"
#include "ar1/assert.hpp"
#include "ar1/task.hpp"
#include "elapsed_timer.hpp"
#include "sequence_generator.hpp"
#include "ar1/scatter.hpp"
#include "atest.hpp"
#include <iostream>

using namespace ar1;


static task<void> test_task1()
{
    auto gen0 = sequence_generator_nt(10'000'000);

    scatter s(gen0);
    scatter_end gen1(s);
    scatter_end gen2(s);

    std::uint64_t check1 = 0;
    std::uint64_t check2 = 0;

    ATEST_TIME("scatter2",
    for (;;)
    {
        auto r1 = co_await gen1;
        if (r1)
        {
            check1 += *r1;
        }
        else
        {
            break;
        }

        auto r2 = co_await gen2;
        if (r2)
        {
            check2 += *r2;
        }
        else
        {
            break;
        }
    }
    )

    ATEST_ASSERT(check1 == 49999995000000 && check2 == 49999995000000)
}

static task<void> test_task2()
{
    auto gen0 = sequence_generator_d(10);

    scatter s(gen0);
    scatter_end gen1(s);
    scatter_end gen2(s);

    std::uint64_t check1 = 0;
    std::uint64_t check2 = 0;

    for (;;)
    {
        auto r1 = co_await gen1;
        if (r1)
        {
            check1 += *r1;
        }
        else
        {
            break;
        }

        auto r2 = co_await gen2;
        if (r2)
        {
            check2 += *r2;
        }
        else
        {
            break;
        }
    }

    ATEST_ASSERT(check1 == 45 && check2 == 45)

    co_return;
}

static task<void> test_task3()
{
    auto gen1 = sequence_generator_nt(100);

    scatter s(gen1);
    scatter_end e1(s);
    scatter_end e2(s);

    auto r1 = co_await e1;
    ATEST_ASSERT(r1 && *r1 == 0);
    auto r2 = co_await e2;
    ATEST_ASSERT(r2 && *r2 == 0);
    co_await e1;
    auto r3 = co_await e1;
    ATEST_ASSERT(r3 && *r3 == 2);
    auto r4 = co_await e2;
    ATEST_ASSERT(r4 && *r4 == 1);

    for (int i = 0; i < 10; ++i)
    {
        co_await e1;
    }

    auto r5 = co_await e1;
    ATEST_ASSERT(r5 && *r5 == 13);
    ATEST_MUST_THROW(co_await e2;)

    ar1_finish(e1);
    ar1_finish(e2);

    co_return;
}

static task<void> all_tests()
{
    co_await test_task1();
    co_await test_task2();
    co_await test_task3();
    co_return;
}


int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    basio::thread loop;
    loop.set_name("main");
    ar1::default_executor_set(loop);

    auto all = all_tests();
    ar1::await(all, ar1::functor_with_executor{[&](){ loop.stop(); }, loop});

    loop.run_for(std::chrono::seconds(10));
    AR1_ASSERT(all.finished());;

    return 0;
}
