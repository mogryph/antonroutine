#include "ar1/boost/thread.hpp"
#include "ar1/task.hpp"
#include "elapsed_timer.hpp"
#include "sequence_generator.hpp"
#include "plus_one_filter.hpp"
#include "atest.hpp"
#include <iostream>

using namespace ar1;


static task<void> test_task1()
{
    auto gen = sequence_generator_nt(10'000'000);

    std::uint64_t check = 0;

    ATEST_TIME("NT gen, copy result",
        while (auto v = co_await gen)
        {
            check += *v;
        }
    )

    ATEST_CHECK(check, 49999995000000);

    co_return;
}

static task<void> test_task2()
{
    auto gen = sequence_generator(10'000'000);

    std::uint64_t check = 0;

    ATEST_TIME("Task gen, copy result",
        while (auto v = co_await gen)
        {
            check += v;
        }
    )

    ATEST_CHECK(check, 49999995000000);

    co_return;
}

static task<void> test_task3()
{
    auto gen = sequence_generator(10'000'000);

    std::uint64_t check = 0;

    ATEST_TIME("Task gen, ref result",
        while (auto v = co_await gen)
        {
            check += v;
        }
    )

    ATEST_CHECK(check, 49999995000000);

    co_return;
}

static task<void> test_task4()
{
    auto gen1 = sequence_generator_nt(10'000'000);
    auto gen = plus_one_filter_nt(gen1);

    std::uint64_t check = 0;

    ATEST_TIME("NT gen, POF",
        while (auto v = co_await gen)
        {
            check += *v;
        }
    )

    ATEST_CHECK(check, 50000005000000);

    co_return;
}

static task<void> test_task5()
{
    auto gen1 = sequence_generator(10'000'000);
    auto gen = plus_one_filter(gen1);

    std::uint64_t check = 0;

    ATEST_TIME("Task gen, POF",
        while (auto v = co_await gen)
        {
            check += v;
        }
    )

    ATEST_CHECK(check, 50000004999999);

    co_return;
}

static task<void> all_tests()
{
    co_await test_task1();
    co_await test_task2();
    co_await test_task3();
    co_await test_task4();
    co_await test_task5();
}


int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    basio::thread loop;
    loop.set_name("main");
    ar1::default_executor_set(executor::instant());

    auto all = all_tests();
    ar1::await(all, ar1::functor_with_executor{[&](){ loop.stop(); }, loop});

    loop.run_for(std::chrono::seconds(20));
    AR1_ASSERT(all.finished());

    return 0;
}
