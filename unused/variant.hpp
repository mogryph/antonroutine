#ifndef VARIANT_HPP
#define VARIANT_HPP

#include <utility>


namespace ar1
{


template <typename T0, typename T1>
class variant
{
public:

    ~variant()
    {
        deinitialize();
    }

    variant()
    {
        emplace<0>(T0{}); // TODO
    }

//    variant(const T0& v)
//    {
//        emplace<0>(v);
//    }

//    variant(const T1& v)
//    {
//        emplace<1>(v);
//    }

    variant(const variant& other)
    {
        if (other.m_index == 0)
        {
            emplace<0>(other.m_v0);
        }
        else if (other.m_index == 1)
        {
            emplace<1>(other.m_v1);
        }
        else
        {
            emplace<0>(T0{}); // TODO
        }
    }

    variant(variant&& other) noexcept
    {
        if (other.m_index == 0)
        {
            emplace<0>(std::move(other.m_v0));
        }
        else if (other.m_index == 1)
        {
            emplace<1>(std::move(other.m_v1));
        }
        else
        {
            emplace<0>(T0{}); // TODO
        }
    }

    variant& operator=(const variant& other)
    {
        deinitialize();

        if (other.m_index == 0)
        {
            emplace<0>(other.m_v0);
        }
        else if (other.m_index == 1)
        {
            emplace<1>(other.m_v1);
        }
        else
        {
            emplace<0>(T0{}); // TODO
        }

        return *this;
    }

    variant& operator=(variant&& other) noexcept
    {
        deinitialize();

        if (other.m_index == 0)
        {
            emplace<0>(std::move(other.m_v0));
        }
        else if (other.m_index == 1)
        {
            emplace<1>(std::move(other.m_v1));
        }
        else
        {
            emplace<0>(T0{}); // TODO
        }

        return *this;
    }

    std::size_t index() const
    {
        return m_index;
    }

    template <std::size_t Index>
    auto& get()
    {
        if constexpr (Index == 0)
        {
            return m_v0;
        }
        else if constexpr (Index == 1)
        {
            return m_v1;
        }
    }

    template <std::size_t Index, typename T>
    void emplace(T&& value)
    {
        if constexpr (Index == 0)
        {
            m_index = 0;
            new (&m_v0) T0(std::forward<T>(value));
        }
        else if constexpr (Index == 1)
        {
            m_index = 1;
            new (&m_v1) T1(std::forward<T>(value));
        }
    }

private:

    void deinitialize()
    {
        if (m_index == 0)
        {
            m_v0.~T0();
        }
        else if (m_index == 1)
        {
            m_v1.~T1();
        }
    }

private:
    std::size_t m_index;
    union
    {
        T0 m_v0;
        T1 m_v1;
    };
};


} // namespace ar1


namespace std
{

template <std::size_t Index, typename T0, typename T1>
auto& get(ar1::variant<T0, T1>& v)
{
    return v.template get<Index>();
}

} // namespace std


#endif // VARIANT_HPP
