#ifndef ANTONPTIONAL_HPP
#define ANTONPTIONAL_HPP

#include <optional> // std::nullopt_t


template <typename T>
struct antonptional_type;

template <>
struct antonptional_type<double>
{
    constexpr static double invalid = std::numeric_limits<double>::quiet_NaN();
};


template <typename T>
class antonptional
{
public:

    antonptional()
        : m_value(antonptional_type<T>::invalid)
    {

    }

    antonptional(T value)
        : m_value(value)
    {
    }

    antonptional(std::nullopt_t)
        : m_value(antonptional_type<T>::invalid)
    {
    }

    antonptional& operator=(T value)
    {
        m_value = value;

        return *this;
    }

    antonptional& operator=(std::nullopt_t)
    {
        m_value = antonptional_type<T>::invalid;
        return *this;
    }

    T& operator*()
    {
        return m_value;
    }

    operator bool() const
    {
        return m_value != antonptional_type<T>::invalid;
    }

private:
    T m_value;
};


#endif // ANTONPTIONAL_HPP
