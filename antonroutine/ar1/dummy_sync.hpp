#ifndef AR1_DUMMY_SYNC_HPP
#define AR1_DUMMY_SYNC_HPP

#include "assert.hpp"
#include <atomic>


namespace ar1
{


template <typename T>
class dummy_atomic;

template <>
class dummy_atomic<bool>
{
public:
    explicit dummy_atomic(bool value)
        : m_value(value)
    {

    }

    bool exchange(bool desired, [[maybe_unused]] std::memory_order order)
    {
        std::swap(m_value, desired);

        return desired;
    }

private:
    bool m_value;
};


class dummy_mutex
{
public:
    dummy_mutex()
        : m_locked(false)
    {

    }

    void lock()
    {
        AR1_ASSERT(!m_locked);
        m_locked = true;
    }

    void unlock()
    {
        AR1_ASSERT(m_locked);
        m_locked = false;
    }

private:
    bool m_locked;
};


} // namespace ar1


#endif // AR1_DUMMY_SYNC_HPP
