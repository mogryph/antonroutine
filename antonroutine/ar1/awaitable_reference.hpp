#ifndef AR1_AWAITABLE_REFERENCE_HPP
#define AR1_AWAITABLE_REFERENCE_HPP

#include "awaitable.hpp"


namespace ar1
{


template <typename T>
class awaitable_reference
{
public:

    awaitable_reference(T& a)
        : m_a(a)
    {
    }

    [[nodiscard]] bool await_ready() const
    {
        return m_a.await_ready();
    }

    template <typename Continuation>
    [[nodiscard]] bool await_suspend([[maybe_unused]] Continuation&& continuation_)
    {
        return m_a.await_suspend(std::forward<Continuation>(continuation_));
    }

    decltype(auto) await_resume()
    {
        return m_a.await_resume();
    }

    [[nodiscard]] bool finished() const
    {
        return m_a.finished();
    }

    void cancel()
    {
        m_a.cancel();
    }

private:
    T& m_a;
};


static_assert(UnifiedAwaitable<awaitable_reference<dummy_awaitable>>);


} // namespace ar1


#endif // AR1_AWAITABLE_REFERENCE_HPP
