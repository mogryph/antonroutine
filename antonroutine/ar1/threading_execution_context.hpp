#ifndef AR1_THREADING_EXECUTION_CONTEXT_HPP
#define AR1_THREADING_EXECUTION_CONTEXT_HPP

#include "execution.hpp"
#include <iostream>
#include <thread>

namespace ar1
{


inline int thread_hint()
{
    return gettid() % 100;
}


class threading_execution_context : public execution_context
{
public:
    void execute(std::function<void ()> f) override
    {
        std::thread t([f = std::move(f)]()
        {
            std::cout << "[threading_executor] Thread started " << thread_hint() << std::endl;
            f();
            std::cout << "[threading_executor] Thread finished " << thread_hint() << std::endl;
        });
        t.detach();
    }
};


} // namespace ar1


#endif // AR1_THREADING_EXECUTION_CONTEXT_HPP
