#ifndef AR1_SCATTER_HPP
#define AR1_SCATTER_HPP

#include "awaitable.hpp"
#include "assert.hpp"
#include <array>
#include <set>

#include <iostream>


namespace ar1
{

class observer // TODO Temporary
{
public:
    virtual void operator()() = 0;
};

/*
 * TODO Reimplement.
 * TODO Thread-safe version (using channels?).
 */
template <typename Source>
class scatter
{
    using source_resume_type = typename awaitable_traits<Source>::resume_type;
public:

    class no_longer_available_exception : public std::exception
    {
    public:
    };

    scatter(Source& source)
        : m_source(source)
        , m_next(0)
        , m_suspended(false)
    {
    }

    void add_end(observer* e)
    {
        m_ends.insert(e);
    }

    void remove_end(observer* e)
    {
        m_ends.erase(e);
    }

//    bool is_ready(std::uint64_t id) const
//    {
//        if (id < m_next && id + 10 >= m_next) // TODO 10
//        {
//            return true;
//        }
//        else if (id == m_next)
//        {
//            return m_source.await_ready();
//        }
//        else [[unlikely]]
//        {
//            throw no_longer_available_exception{};
//        }
//    }

    bool request(std::uint64_t id, executor executor_)
    {
        if (id < m_next && id + 10 >= m_next) // TODO 10
        {
            return false;
        }
        else if (id == m_next)
        {
            if (m_suspended)
            {
                return true;
            }

            m_suspended = true;

            if (m_source.await_ready())
            {
                return false;
            }

            return m_source.await_suspend(functor_with_executor{[this](){ h(); }, executor_});
        }
        else [[unlikely]]
        {
            throw no_longer_available_exception{};
        }
    }

    const source_resume_type& get(std::uint64_t id)
    {
        m_suspended = false;

        if (id < m_next && id + 10 >= m_next) // TODO 10
        {
            return m_cached[id % m_cached.size()];
        }
        else if (id == m_next)
        {
            m_cached[id % m_cached.size()] = std::move(m_source.await_resume());
            m_next += 1;

            return m_cached[id % m_cached.size()];
        }
        else [[unlikely]]
        {
            throw no_longer_available_exception{};
        }
    }

    void h()
    {
        for (observer* e : m_ends) // TODO May cause destruction of the scatter itself.
        {
            e->operator()();
        }
    }

    bool finished() const
    {
        return m_source.finished();
    }

private:
    Source& m_source;
    std::uint64_t m_next;
    bool m_suspended;
    std::set<observer*> m_ends;
    std::array<source_resume_type, 10> m_cached; // TODO 10
};


template <typename Source>
class scatter_end : public observer
{
    using source_resume_type = typename awaitable_traits<Source>::resume_type;
public:

    ~scatter_end()
    {
        AR1_ASSERT(finished());
        m_s.remove_end(this);
    }

    scatter_end(scatter<Source>& s)
        : m_s(s)
        , m_next(0)
    {
        m_s.add_end(this);
    }

    [[nodiscard]] bool await_ready() const
    {
        return false;
    }

    template <typename Continuation>
    [[nodiscard]] bool await_suspend(Continuation&& continuation_)
    {
        const bool suspended = m_s.request(m_next, continuation_traits<std::decay_t<Continuation>>::get_executor(continuation_));
        if (suspended)
        {
            m_continuation = std::forward<Continuation>(continuation_);
        }

        return suspended;
    }

    const source_resume_type& await_resume()
    {
        m_next += 1;

        return m_s.get(m_next - 1);
    }

    [[nodiscard]] bool finished() const
    {
        return m_s.finished(); // TODO Wrong, check cache.
    }

private:
    void operator()() override
    {
        m_continuation();
    }

private:
    scatter<Source>& m_s;
    std::uint64_t m_next;
    continuation m_continuation;
};


}


#endif // AR1_SCATTER_HPP
