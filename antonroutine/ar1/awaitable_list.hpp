#ifndef AR1_AWAITABLE_LIST_HPP
#define AR1_AWAITABLE_LIST_HPP

#include "awaitable.hpp"
#include "any_awaitable.hpp"
#include <vector>


namespace ar1
{

/*
 * Временный ограниченный класс. Пока не придумаю что-то получше.
 * Хранит таски, пока они не завершатся. С илдящими тасками пока не работает.
 */
class awaitable_list
{
public:

    ~awaitable_list()
    {
        if (!m_list.empty())
        {
            std::cerr << "WARNING! Destroying a list with unfinished awaitables!" << std::endl;
        }
    }

    awaitable_list(executor executor_)
        : m_executor(executor_)
    {

    }

    awaitable_list(const awaitable_list& other) = delete;
    awaitable_list(awaitable_list&& other) noexcept = delete;
    awaitable_list& operator=(const awaitable_list& other) = delete;
    awaitable_list& operator=(awaitable_list&& other) noexcept = delete;

    void add(any_awaitable<void> a)
    {
        m_list.push_back(std::move(a));
        m_list.back().await_suspend(functor_with_executor{[this]()
        {
            something_finished();
        }, m_executor});
    }

    std::size_t size() const
    {
        return m_list.size();
    }

private:
    void something_finished()
    {
        m_list.erase(std::remove_if(m_list.begin(), m_list.end(),
                                    [](const any_awaitable<void>& a) { return a.finished(); }),
                     m_list.end());
    }

private:
    executor m_executor;
    std::vector<any_awaitable<void>> m_list;
};


} // namespace ar1


#endif // AR1_AWAITABLE_LIST_HPP
