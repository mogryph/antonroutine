#ifndef AR1_TASK_HPP
#define AR1_TASK_HPP

#include "assert.hpp"
#include "awaitable.hpp"
#include <coroutine>
#include <memory> // std::addressof
#include <atomic>

#include <iostream> // TODO Remove.


namespace ar1
{

namespace detail
{

template <typename T>
struct is_coroutine_handle : std::false_type {};

template <typename PromiseType>
struct is_coroutine_handle<std::coroutine_handle<PromiseType>> : std::true_type {};

} // namespace detail

executor default_executor_get();
void default_executor_set(executor executor_);


// =====================================================================================================================
//   Final awaitable.
// =====================================================================================================================

struct final_awaitable
{
    bool await_ready() const noexcept
    {
        return false;
    }

    template <typename PromiseType>
    void await_suspend([[maybe_unused]] std::coroutine_handle<PromiseType> handle) noexcept
    {
        auto& promise = handle.promise();

        if (promise.m_state.exchange(true, std::memory_order_acq_rel))
        {
            promise.m_continuation();
        }
    }

    void await_resume() noexcept
    {
    }
};


template <typename Return>
struct promise;


// =====================================================================================================================
//   Promise container.
// =====================================================================================================================

template <typename Return>
struct promise_container
{
    Return& value()
    {
        return m_value;
    }

    void return_value(Return&& value) noexcept
    {
        m_value = std::forward<Return>(value);
    }

    final_awaitable yield_value(const Return& value) noexcept
    {
        m_value = value;

        //auto* self = static_cast<promise<Return>*>(this);

        return final_awaitable{};
    }

    final_awaitable yield_value(Return&& value) noexcept
    {
        m_value = std::forward<Return>(value);

        //auto* self = static_cast<promise<Return>*>(this);

        return final_awaitable{};
    }

    Return m_value;
};

template <typename Return>
struct promise_container<Return&>
{
    Return& value()
    {
        return *m_value;
    }

    void return_value(Return& value) noexcept
    {
        m_value = std::addressof(value);
    }

    final_awaitable yield_value(Return& value) noexcept
    {
        m_value = std::addressof(value);

        //auto* self = static_cast<promise<Return&>*>(this);

        return final_awaitable{};
    }

    Return* m_value;
};

template <>
struct promise_container<void>
{
    void value()
    {
    }

    void return_void() noexcept
    {
    }
};


// =====================================================================================================================
//   Promise.
// =====================================================================================================================

template <typename Return>
class task;

template <typename Return>
struct promise : promise_container<Return>
{
    using atomic_bool_type = dummy_atomic<bool>;
    using mutex_type = dummy_mutex;

    promise() noexcept
        : m_bound_executor(default_executor_get())
        , m_state(false)
    {
    }
    promise(const promise& other) = delete;
    promise(promise&& other) noexcept = delete;
    promise& operator=(const promise& other) = delete;
    promise& operator=(promise&& other) noexcept = delete;

    task<Return> get_return_object() noexcept;

    // TODO (MED). Understand.
    std::suspend_always initial_suspend() const noexcept
    {
        return {};
    }

    // TODO (MED). Understand.
    final_awaitable final_suspend() noexcept
    {
        return final_awaitable{};
    }

    void unhandled_exception() noexcept
    {
        std::cout << "Exception!" << std::endl;
        std::terminate();
        //m_exception = std::current_exception(); // TODO (MED). Implement.
    }

    executor m_bound_executor;
    continuation m_continuation;
    std::atomic<bool> m_state;
    //std::exception_ptr m_exception;
};


// =====================================================================================================================
//   Task.
// =====================================================================================================================

template <typename Return>
class task
{
public:

    using promise_type = promise<Return>;

    ~task()
    {
        if (m_coroutine)
        {
            if (!finished())
            {
                std::cerr << "WARNING! Destroying an unfinished task!" << std::endl;
            }
            m_coroutine.destroy();
        }
    }

    task() noexcept
        : m_coroutine(nullptr)
    {
    }

    explicit task(promise_type& promise) noexcept
        : m_coroutine(std::coroutine_handle<promise_type>::from_promise(promise))
    {
    }

    task(const task& other) = delete;

    task(task&& other) noexcept
        : m_coroutine(other.m_coroutine)
    {
        other.m_coroutine = nullptr;
    }

    task& operator=(const task& other) = delete;

    task& operator=(task&& other) noexcept
    {
        m_coroutine = other.m_coroutine;
        other.m_coroutine = nullptr;

        return *this;
    }

    [[nodiscard]] bool await_ready() const noexcept
    {
        return !m_coroutine || m_coroutine.done();
    }

    template <typename Continuation>
    [[nodiscard]] bool await_suspend(Continuation&& continuation_) noexcept
    {
        auto& promise = m_coroutine.promise();

        promise.m_state.exchange(false, std::memory_order_relaxed);

        promise.m_continuation = std::forward<Continuation>(continuation_); // TODO Slow. Only save if needed.

        promise.m_bound_executor.execute(m_coroutine);

        return !promise.m_state.exchange(true, std::memory_order_acq_rel);
    }

    decltype(auto) await_resume() // TODO (LOW). rval overload
    {
        return m_coroutine.promise().value(); // TODO (LOW). Move or ref?
    }

    bool finished() const
    {
        return m_coroutine.done();
    }

    void cancel()
    {
        // TODO Implement.
    }

    void bind_executor(executor executor_)
    {
        m_coroutine.promise().m_bound_executor = std::move(executor_);
    }

private:
    std::coroutine_handle<promise_type> m_coroutine;
};

template <typename Return>
task<Return> promise<Return>::get_return_object() noexcept
{
    return task<Return>{*this};
}

static_assert(UnifiedAwaitable<task<void>>);



template <typename Return>
struct continuation_traits<std::coroutine_handle<promise<Return>>>
{
    static auto& get_executable(std::coroutine_handle<promise<Return>>& continuation_)
    {
        return continuation_;
    }

    static executor get_executor(std::coroutine_handle<promise<Return>>& continuation_)
    {
        return continuation_.promise().m_bound_executor;
    }
};


#define ar1_finish(ar1_t) while(!ar1_t.finished()) { co_await ar1_t; }

}


#endif // AR1_TASK_HPP
