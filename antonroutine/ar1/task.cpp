#include "task.hpp"


namespace ar1
{


namespace
{
    executor DEFAULT_EXECUTOR; // NOLINT
} // namespace


executor default_executor_get()
{
    return DEFAULT_EXECUTOR;
}

void default_executor_set(executor executor_)
{
    DEFAULT_EXECUTOR = std::move(executor_);
}


} // namespace ar1
