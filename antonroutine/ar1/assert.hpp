#ifndef AR1_ASSERT_HPP
#define AR1_ASSERT_HPP

#include <iostream>


#define AR1_ASSERT(cond)                                            \
    do                                                              \
    {                                                               \
        if (!(cond)) [[unlikely]]                                   \
        {                                                           \
            std::cerr << "ASSERTION FAILED:" << std::endl;          \
            std::cerr << __FILE__ << ":" << __LINE__ << std::endl;  \
            std::cerr << #cond << std::endl;                        \
            std::abort();                                           \
        }                                                           \
    } while(false);                                                 \


#endif // AR1_ASSERT_HPP
