#ifndef AR1_GATHER_2_HPP
#define AR1_GATHER_2_HPP

#include "awaitable.hpp"
#include "assert.hpp"
#include "any_awaitable.hpp"
#include "execution.hpp"
#include <optional>
#include <variant>
#include <vector>


namespace ar1
{


class gather2
{
public:

    ~gather2()
    {
        AR1_ASSERT(finished()); // TODO (MED). Temporary.
    }

    gather2()
    {
    }

    gather2(const gather2& other) = delete;
    gather2(gather2&& other) noexcept = delete;
    gather2& operator=(const gather2& other) = delete;
    gather2& operator=(gather2&& other) noexcept = delete;

    bool await_ready() const
    {
        return false;
    }

    template <typename Continuation>
    bool await_suspend(Continuation continuation_)
    {
        continuation continuation1 = std::move(continuation_); // TODO Only save if suspended.

        bool something_ready = false;
        bool all_finished = true;

        for (std::size_t i = 0; i < m_sources.size(); ++i)
        {
            auto& source = m_sources[i];
            auto& state = m_source_states[i];

            if (state == source_state::unknown)
            {
                // TODO(CRIT). Handle awaitable properly.

                if (source.await_ready())
                {
                    state = source_state::ready;
                }
                else
                {
                    const bool suspended = source.await_suspend(functor_with_executor{[this, index = i]()
                    {
                        h(index);
                    }, continuation1.m_executor});

                    if (suspended)
                    {
                        state = source_state::not_ready;
                    }
                    else
                    {
                        state = source_state::ready;
                    }
                }
            }

            something_ready = something_ready || state == source_state::ready;
            all_finished = all_finished && state == source_state::finished;
        }

        if (something_ready || all_finished)
        {
            return false;
        }
        else
        {
            AR1_ASSERT(!m_continuation);
            m_continuation = std::move(continuation1);

            return true;
        }
    }

    std::optional<std::size_t> await_resume()
    {
        if (finished())
        {
            return std::nullopt;
        }

        std::optional<std::size_t> result;

        bool something_resumed = false;

        for (std::size_t i = 0; i < m_sources.size(); ++i)
        {
            auto& source = m_sources[i];
            auto& state = m_source_states[i];

            if (!something_resumed && state == source_state::ready)
            {
                source.await_resume();

                if (source.finished())
                {
                    state = source_state::finished;
                }
                else
                {
                    state = source_state::unknown;
                }

                result = i;

                something_resumed = true;
            }
        };

        AR1_ASSERT(something_resumed);

        return result;
    }

    bool finished() const
    {
        bool f = true;

        for (std::size_t i = 0; i < m_sources.size(); ++i)
        {
            auto& state = m_source_states[i];
            f = f && state == source_state::finished;
        }

        return f;
    }

    void cancel()
    {

    }

    void cancel_all()
    {
        for (std::size_t i = 0; i < m_sources.size(); ++i)
        {
            auto& source = m_sources[i];
            source.cancel();
        };
    }

    void add(any_awaitable<void> source)
    {
        m_sources.push_back(std::move(source));
        m_source_states.push_back(source_state::unknown);

        // TODO Resume gather or suspend source.
    }

private:
    void h(std::size_t index)
    {
        auto& state = m_source_states[index];
        AR1_ASSERT(state == source_state::not_ready);
        state = source_state::ready;

        continuation continuation_;
        std::swap(m_continuation, continuation_);
        if (continuation_)
        {
            continuation_();
        }
    }

private:
    enum class source_state
    {
        unknown, ready, not_ready, finished,
    };


    std::vector<any_awaitable<void>> m_sources;
    std::vector<source_state> m_source_states;
    continuation m_continuation;
};

static_assert(UnifiedAwaitable<gather2>);


} // namespace ar1


#endif // AR1_GATHER_2_HPP
