#ifndef AR1_EVENT_HPP
#define AR1_EVENT_HPP

#include "awaitable.hpp"
#include "assert.hpp"


namespace ar1
{


class event
{
public:

    event()
        : m_set(false)
    {
    }

    event(const event& other) = delete;
    event(event&& other) noexcept = delete;
    event& operator=(const event& other) = delete;
    event operator=(event&& other) noexcept = delete;

    [[nodiscard]] bool await_ready() const
    {
        return m_set;
    }

    template <typename Continuation>
    [[nodiscard]] bool await_suspend([[maybe_unused]] Continuation&& continuation_)
    {
        m_continuation = std::forward<Continuation>(continuation_);

        return true;
    }

    int await_resume()
    {
        return 0;
    }

    [[nodiscard]] bool finished() const
    {
        return false;
    }

    void cancel()
    {

    }

    void set(bool v)
    {
        m_set = v;

        if (m_set)
        {
            m_continuation();
        }
    }

private:
    bool m_set;
    continuation m_continuation;
};

static_assert(UnifiedAwaitable<event>);


} // namespace ar1


#endif // AR1_EVENT_HPP
