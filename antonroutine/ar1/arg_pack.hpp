#ifndef BRA_ARG_PACK_HPP
#define BRA_ARG_PACK_HPP

#include <utility>
#include <tuple>


namespace bra
{


template <typename ...Args>
class arg_pack
{
private:

    template <std::size_t Number, typename T>
    struct Element
    {
        constexpr static std::size_t number = Number;
        using type = T;
    };

    template <typename Functor, std::size_t Number>
    constexpr static void for_each_([[maybe_unused]] const Functor& f)
    {
    }

    template <typename Functor, std::size_t Number, typename First, typename ...Rest>
    constexpr static void for_each_(const Functor& f)
    {
        f.template operator()<Element<Number, First>>();
        for_each_<Functor, Number + 1, Rest...>(f);
    }

    template <typename Predicate, std::size_t Number, typename ...Args1>
    struct find_;

    template <typename Predicate, std::size_t Number>
    struct find_<Predicate, Number>
    {
        using type = void;
    };

    template <typename Predicate, std::size_t Number, typename First, typename ...Rest>
    struct find_<Predicate, Number, First, Rest...>
    {
        using current_element = Element<Number, First>;
        using rest_element = typename find_<Predicate, Number + 1, Rest...>::type;
        constexpr static bool current_ok = Predicate::template matched<current_element>;
        constexpr static bool rest_not_ok = std::is_same_v<rest_element, void>;

        using type = std::conditional_t<current_ok,
                                        typename std::conditional<rest_not_ok, current_element, void>::type,
                                        rest_element>;
    };

public:

    template <typename Functor>
    constexpr static void for_each(const Functor& f)
    {
        for_each_<Functor, 0, Args...>(f);
    }

    template <typename Predicate>
    using find = typename find_<Predicate, 0, Args...>::type;
};


template <typename DesiredType>
struct predicate_example
{
    template <typename Element>
    constexpr static bool matched = std::is_same_v<typename Element::type, DesiredType>;
};


template <std::size_t index, typename T, typename ...Ts, typename std::enable_if<index == 0>::type* = nullptr>
inline constexpr decltype(auto) get_arg(T&& t, Ts&&... ts)
{
    return std::forward<T>(t);
}

template <std::size_t index, typename T, typename ...Ts, typename std::enable_if<(index > 0 && index <= sizeof...(Ts))>::type* = nullptr>
inline constexpr decltype(auto) get_arg(T&& t, Ts&&... ts)
{
    return get_arg<index - 1>(std::forward<Ts>(ts)...);
}

template <long long index, typename ...Ts>
inline constexpr bool get_arg_index_ok()
{
    return index >= 0 && index < (long long)sizeof...(Ts);
}

template <long long index, typename T, typename ...Ts, typename std::enable_if<(!get_arg_index_ok<index, T, Ts...>())>::type* = nullptr>
inline constexpr decltype(auto) get_arg(T&& t, Ts&&... ts)
{
    static_assert(get_arg_index_ok<index, T, Ts...>(), "Bad index.");
    //return std::forward<T>(t);
}


template <typename T>
struct get_arg_pack;

template <typename ...Args>
struct get_arg_pack<std::tuple<Args...>>
{
    using type = arg_pack<Args...>;
};

template <typename Functor, typename ...Args>
void tuple_for_each(const std::tuple<Args...>& t, const Functor& f)
{
    arg_pack<Args...>::for_each([&]<typename Element>()
    {
        f(std::get<Element::number>(t));
    });
}

template <typename Functor, typename ...Args>
void tuple_for_each(std::tuple<Args...>& t, const Functor& f) // TODO Merge with const version.
{
    arg_pack<Args...>::for_each([&]<typename Element>()
    {
        f(std::get<Element::number>(t));
    });
}



} // namespace bra


#endif // BRA_ARG_PACK_HPP
