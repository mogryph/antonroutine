#ifndef AR1_ANY_AWAITABLE_HPP
#define AR1_ANY_AWAITABLE_HPP

#include "awaitable.hpp"


namespace ar1
{


template <typename Return>
class any_awaitable_base
{
public:
    virtual ~any_awaitable_base() = default;
    virtual bool await_ready() const = 0;
    virtual bool await_suspend1(continuation continuation_) = 0;
    virtual Return await_resume() = 0;
    virtual bool finished() const = 0;
    virtual void cancel() = 0;
};


template <typename T, typename Return>
class any_awaitable_t : public any_awaitable_base<Return>
{
public:
    any_awaitable_t(T a)
        : m_a(std::move(a))
    {
    }

    template <typename ...Args>
    any_awaitable_t(Args&&... args)
        : m_a(std::forward<Args>(args)...)
    {
    }

    any_awaitable_t(const any_awaitable_t& other) = delete;
    any_awaitable_t(any_awaitable_t&& other) noexcept = delete;
    any_awaitable_t& operator=(const any_awaitable_t& other) = delete;
    any_awaitable_t& operator=(any_awaitable_t&& other) noexcept = delete;

    bool await_ready() const override
    {
        return m_a.await_ready();
    }

    bool await_suspend1(continuation continuation_) override
    {
        return m_a.await_suspend(std::move(continuation_));
    }

    Return await_resume() override
    {
        return m_a.await_resume();
    }

    bool finished() const override
    {
        return m_a.finished();
    }

    void cancel() override
    {
        m_a.cancel();
    }

private:
    T m_a;
};


template <typename Return>
class any_awaitable
{
public:

    ~any_awaitable()
    {
        reset();
    }

    template <typename T>
    any_awaitable(T a)
        : m_b(new any_awaitable_t<T, Return>(std::move(a)))
    {
    }

    template <typename T, typename ...Args>
    any_awaitable(std::in_place_type_t<T>, Args&&... args)
        : m_b(new any_awaitable_t<T, Return>(std::forward<Args>(args)...))
    {
    }

    any_awaitable(const any_awaitable& other) = delete;
    any_awaitable(any_awaitable&& other) noexcept
        : m_b(other.m_b)
    {
        other.m_b = nullptr;
    }
    any_awaitable& operator=(const any_awaitable& other) = delete;
    any_awaitable& operator=(any_awaitable&& other) noexcept
    {
        reset();

        m_b = other.m_b;
        other.m_b = nullptr;

        return *this;
    }

    bool await_ready() const
    {
        return m_b->await_ready();
    }

    template <typename Continuation>
    bool await_suspend(Continuation&& continuation_)
    {
        return m_b->await_suspend1(std::forward<Continuation>(continuation_));
    }

    Return await_resume()
    {
        return m_b->await_resume();
    }

    bool finished() const
    {
        return m_b->finished();
    }

    void cancel()
    {
        m_b->cancel();
    }

private:
    void reset()
    {
        if (m_b != nullptr)
        {
            delete m_b;
            m_b = nullptr;
        }
    }

    any_awaitable_base<Return>* m_b;
};

static_assert(UnifiedAwaitable<any_awaitable<void>>);


template <typename T, typename ...Args>
auto make_any_awaitable(Args&&... args)
{
    using Return = typename awaitable_traits<T>::resume_type;

    return any_awaitable<Return>(std::in_place_type<T>, std::forward<Args>(args)...);
}


}


#endif // AR1_ANY_AWAITABLE_HPP
