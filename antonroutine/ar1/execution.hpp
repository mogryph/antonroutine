#ifndef AR1_EXECUTION_HPP
#define AR1_EXECUTION_HPP

#include "assert.hpp"
#include <functional>


namespace ar1
{


class execution_context
{
public:
    virtual ~execution_context() = default;
    virtual void execute(std::function<void ()> f) = 0;
};


class executor
{
public:
    ~executor()
    {
    }

    executor()
        : m_context(nullptr)
    {
    }

    executor(execution_context& context)
        : m_context(&context)
    {
    }

    executor(const executor& other)
        : m_context(other.m_context)
    {
    }

    executor(executor&& other) noexcept
        : m_context(other.m_context)
    {
        other.m_context = nullptr;
    }

    executor& operator=(const executor& other)
    {
        m_context = other.m_context;

        return *this;
    }

    executor& operator=(executor&& other) noexcept
    {
        m_context = other.m_context;
        other.m_context = nullptr;

        return *this;
    }

    static executor instant()
    {
        executor e;
        e.m_context = reinterpret_cast<execution_context*>(1);

        return e;
    }

    template <typename Functor>
    void execute(Functor&& f) const
    {
        AR1_ASSERT(m_context != nullptr); // TODO Dbg only.

        if (m_context == reinterpret_cast<execution_context*>(1))
        {
            f();
        }
        else
        {
            m_context->execute(std::move(f));
        }
    }

    bool operator==(const executor& other) const
    {
        return m_context == other.m_context;
    }

    operator bool() const
    {
        return m_context != nullptr;
    }

private:
    execution_context* m_context;
};



//executor active_executor_get();
//void active_executor_set(executor executor_);
//void active_executor_unset();


} // namespace ar1


#endif // AR1_EXECUTION_HPP
