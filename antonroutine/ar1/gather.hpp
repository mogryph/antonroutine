#ifndef AR1_GATHER_HPP
#define AR1_GATHER_HPP

#include "awaitable.hpp"
#include "arg_pack.hpp"
#include "assert.hpp"
#include "execution.hpp"
#include <optional>
#include <variant>


namespace ar1
{


template <typename ...Sources>
class gather
{
    constexpr static std::size_t num_sources = sizeof...(Sources);
    using yield_type = std::variant<typename awaitable_traits<Sources>::resume_type...>;
public:

    ~gather()
    {
        AR1_ASSERT(finished()); // TODO (MED). Temporary.
    }

    explicit gather(Sources&... sources)
        : m_sources(sources...)
    {
        m_source_states.fill(source_state::unknown);
    }

    gather(const gather& other) = delete;
    gather(gather&& other) noexcept = delete;
    gather& operator=(const gather& other) = delete;
    gather& operator=(gather&& other) noexcept = delete;

    [[nodiscard]] bool await_ready() const
    {
        return false;
    }

    template <typename Continuation> // TODO cont
    [[nodiscard]] bool await_suspend(Continuation&& continuation_)
    {
        continuation continuation1 = std::forward<Continuation>(continuation_); // TODO Only save if suspended.

        bool something_ready = false;
        bool all_finished = true;

        bra::arg_pack<Sources...>::for_each([&]<typename Element>()
        {
            auto& source = std::get<Element::number>(m_sources);
            auto& state = std::get<Element::number>(m_source_states);

            if (state == source_state::unknown)
            {
                // TODO(CRIT). Handle awaitable properly.

                if (source.await_ready())
                {
                    state = source_state::ready;
                }
                else
                {
                    const bool suspended = source.await_suspend(functor_with_executor{[this, index = Element::number]()
                    {
                        h(index);
                    }, continuation1.m_executor});

                    if (suspended)
                    {
                        state = source_state::not_ready;
                    }
                    else
                    {
                        state = source_state::ready;
                    }
                }
            }

            something_ready = something_ready || state == source_state::ready;
            all_finished = all_finished && state == source_state::finished;
        });

        if (something_ready || all_finished)
        {
            return false;
        }
        else
        {
            m_continuation = std::move(continuation1);

            return true;
        }
    }

    std::optional<yield_type> await_resume()
    {
        if (finished())
        {
            return std::nullopt;
        }

        yield_type result;

        bool something_resumed = false;

        bra::arg_pack<Sources...>::for_each([&]<typename Element>()
        {
            auto& source = std::get<Element::number>(m_sources);
            auto& state = std::get<Element::number>(m_source_states);

            if (!something_resumed && state == source_state::ready)
            {
                decltype(auto) r = source.await_resume();

                if (source.finished())
                {
                    state = source_state::finished;
                }
                else
                {
                    state = source_state::unknown;
                }

                result.template emplace<Element::number>(std::move(r));

                something_resumed = true;
            }
        });

        AR1_ASSERT(something_resumed);

        return result;
    }

    [[nodiscard]] bool finished() const
    {
        bool f = true;

        bra::arg_pack<Sources...>::for_each([&]<typename Element>()
        {
            //auto& source = std::get<Element::number>(m_sources);
            auto& state = std::get<Element::number>(m_source_states);
            f = f && state == source_state::finished;
        });

        return f;
    }

    void cancel()
    {

    }

    void cancel_all()
    {
        bra::arg_pack<Sources...>::for_each([&]<typename Element>()
        {
            auto& source = std::get<Element::number>(m_sources);
            source.cancel();
        });
    }

private:
    void h(std::size_t index)
    {
        auto& state = m_source_states[index];
        AR1_ASSERT(state == source_state::not_ready);
        state = source_state::ready;

        m_continuation();
    }

private:
    enum class source_state
    {
        unknown, ready, not_ready, finished,
    };

    std::tuple<Sources&...> m_sources;
    std::array<source_state, num_sources> m_source_states;
    continuation m_continuation;
};

static_assert(UnifiedAwaitable<gather<dummy_awaitable>>);


} // namespace ar1


#endif // AR1_GATHER_HPP
