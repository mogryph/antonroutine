#ifndef AR1_AWAITABLE_HPP
#define AR1_AWAITABLE_HPP

#include "execution.hpp"
#include "dummy_sync.hpp"
#include <utility>


namespace ar1
{


template<typename T>
concept UnifiedAwaitable = requires(T& a)
{
    { std::as_const(a).await_ready() } -> std::same_as<bool>;
    { a.await_suspend([](){}) } -> std::same_as<bool>;
    { a.await_resume() };

    { std::as_const(a).finished() } -> std::same_as<bool>;
    { a.cancel() } -> std::same_as<void>;
};


template <typename T>
struct awaitable_traits
{
    using resume_type = std::remove_reference_t<decltype(std::declval<T&>().await_resume())>;
};


// =====================================================================================================================
//   Continuation.
// =====================================================================================================================

template <typename Continuation>
struct continuation_traits;


struct continuation
{
    continuation()
        : m_set(false)
    {
    }

    template <typename Continuation>
    continuation(Continuation&& continuation_)
        : m_executable(std::move(continuation_traits<std::decay_t<Continuation>>::get_executable(continuation_)))
        , m_executor(std::move(continuation_traits<std::decay_t<Continuation>>::get_executor(continuation_)))
        , m_set(true)
    {
    }

    /*
     * Этот метод может вызываться из другого потока.
     * Этот метод также вызывается при отмене awaitable и может быть вызван когда executable уже в очереди event loop.
     */
    void operator()()
    {
        if (m_set.exchange(false, std::memory_order_acq_rel))
        {
            m_executor.execute(std::move(m_executable));
        }
    }

    std::function<void ()> m_executable;
    executor m_executor;
    dummy_atomic<bool> m_set;
};


// =====================================================================================================================
//   Functor with executor.
// =====================================================================================================================

template <typename FunctorType>
struct functor_with_executor
{
    FunctorType m_executable;
    executor m_executor;
};

template <typename FunctorType>
functor_with_executor(FunctorType&& f, executor executor_) -> functor_with_executor<FunctorType>;

template <typename FunctorType>
struct continuation_traits<functor_with_executor<FunctorType>>
{
    // TODO Это надо улучшить.

    static auto& get_executable(functor_with_executor<FunctorType>&& continuation_)
    {
        return continuation_.m_executable;
    }

    static auto get_executable(const functor_with_executor<FunctorType>& continuation_)
    {
        return continuation_.m_executable;
    }

    static executor& get_executor(functor_with_executor<FunctorType>&& continuation_)
    {
        return continuation_.m_executor;
    }

    static executor get_executor(const functor_with_executor<FunctorType>& continuation_)
    {
        return continuation_.m_executor;
    }
};


// =====================================================================================================================
//   Dummy awaitable.
// =====================================================================================================================

class dummy_awaitable
{
public:
    [[nodiscard]] bool await_ready() const // NOLINT
    {
        return true;
    }

    template <typename Continuation>
    [[nodiscard]] bool await_suspend([[maybe_unused]] Continuation&& continuation_)
    {
        return false;
    }

    int await_resume() // NOLINT
    {
        return 0;
    }

    [[nodiscard]] bool finished() const  // NOLINT
    {
        return true;
    }

    void cancel()
    {

    }
};

static_assert(UnifiedAwaitable<dummy_awaitable>);


template <typename Awaitable, typename Functor>
void await(Awaitable& awaitable, const functor_with_executor<Functor>& fe)
{
    if (awaitable.await_ready() || !awaitable.await_suspend(fe))
    {
        fe.m_executor.execute(std::move(fe.m_executable));
    }
}


} // namespace ar1


#endif // AR1_AWAITABLE_HPP
